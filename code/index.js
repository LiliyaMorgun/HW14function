
let array = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8];
function map(fn, array){
    let arrayNew = [];
	for (i=0; i<array.length; i++) {
	arrayNew [i] = fn(array[i])
	}
	return (arrayNew);
}
document.write("<p>Source array: " + array.join(", ") + "<br>");
document.write("<hr>");

function double (i){
	return i*2;
}
document.write("<p>New array after double every elements: " + map(double,array).join(", ") + "<br>");
document.write("<hr>");

function decrease(i){
	return --i;
}
document.write("<p>New array after decrease every elements: " + map(decrease,array).join(", ") + "<br>");
document.write("<hr>");


function moduloDiv(i){
	if (i % 3 == 0){
	return i;
    }
}
document.write("<p>New array after modulo division: " + map(moduloDiv,array).join(", ") + "<br>");
document.write("<hr>");

	

/*let age = prompt ("Your age: ");

function checkAge (age){
return (age > 18) ? true : confirm('Parents allowed?');
}
let checkAgeAgain = (age);
document.write(checkAge(checkAgeAgain));*/ 

let age = prompt("Your age: ");
function checkAge(age){
return (age > 18) || confirm('Parents allowed?');
}
let checkAgeAgain = (age);
document.write('<h1>Result of check - ' + checkAge(checkAgeAgain));

